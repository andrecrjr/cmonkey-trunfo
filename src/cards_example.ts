export const cards: import("./components/Card/Card").ICard[] = [
  {
    id: 1,
    qt_macaco: 1,
    potassio: 4,
    preco: 19,
    meme: 11,
    effect: "+2 Potássio",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543038847778896/trunfo1.png",
  },
  {
    id: 2,
    qt_macaco: 3,
    potassio: 9,
    preco: 15,
    meme: 10,
    effect: "escolha um status para +2",
    url_image:
      "https://cdn.discordapp.com/attachments/733117754168574002/833082329937084426/Trunfo3.png",
  },
  {
    id: 3,
    qt_macaco: 5,
    potassio: 2,
    preco: 9,
    meme: 12,
    effect: "+1 em qualquer status",
    url_image:
      "https://cdn.discordapp.com/attachments/433102807239884800/833399867786133514/trunfo3.png",
  },
  {
    id: 4,
    qt_macaco: 3,
    potassio: 19,
    preco: 19,
    meme: 7,
    effect: "escolha alguém -2 seu",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543053771112468/trunfo4.png",
  },
  {
    id: 5,
    qt_macaco: 4,
    potassio: 9,
    preco: 15,
    meme: 13,
    effect: "-1 em tudo oponentes",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543059857571860/trunfo5.png",
  },
  {
    id: 6,
    qt_macaco: 8,
    potassio: 19,
    preco: 7,
    meme: 7,
    effect: "+3 em qualquer",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543065871024210/trunfo6.png",
  },
  {
    id: 7,
    qt_macaco: 8,
    potassio: 3,
    preco: 16,
    meme: 12,
    effect: "+3 Potássio ou meme",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543072650199150/trunfo7.png",
  },
  {
    id: 8,
    qt_macaco: 8,
    potassio: 19,
    preco: 10,
    meme: 9,
    effect: "escolha -3 em um e ganhar +2 outro",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543078232293416/trunfo8.png",
  },
  {
    id: 9,
    qt_macaco: 19,
    potassio: 7,
    preco: 9,
    meme: 6,
    effect: "+3 em dois status",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543085353959484/trunfo9.png",
  },
  {
    id: 10,
    qt_macaco: 11,
    potassio: 4,
    preco: 14,
    meme: 8,
    effect: "-1 de um status todos",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543091179978783/trunfo10.png",
  },
  {
    id: 11,
    qt_macaco: 10,
    potassio: 2,
    preco: 19,
    meme: 7,
    effect: "escolha uma pessoa -2 de outra em potássio",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543138462892062/trunfo11.png",
  },
  {
    id: 12,
    qt_macaco: 8,
    potassio: 2,
    preco: 5,
    meme: 10,
    effect: "Troque um status base com outra pessoa",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543144992899072/trunfo12.png",
  },
  {
    id: 13,
    qt_macaco: 7,
    potassio: 4,
    preco: 6,
    meme: 12,
    effect: "-2 de alguém",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543151515303967/trunfo13.png",
  },
  {
    id: 14,
    qt_macaco: 6,
    potassio: 13,
    preco: 12,
    meme: 5,
    effect: "todos perdem -1",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543160679989348/trunfo14.png",
  },
  {
    id: 15,
    qt_macaco: 3,
    potassio: 14,
    preco: 8,
    meme: 13,
    effect: "Duas pessoas -1 meme",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543168312836136/trunfo15.png",
  },
  {
    id: 16,
    qt_macaco: 5,
    potassio: 8,
    preco: 13,
    meme: 9,
    effect: "+3 em qualquer",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543175435026512/trunfo16.png",
  },
  {
    id: 17,
    qt_macaco: 5,
    potassio: 19,
    preco: 5,
    meme: 9,
    effect: "+1 em dois status",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543185648549919/trunfo17.png",
  },
  {
    id: 18,
    qt_macaco: 3,
    potassio: 19,
    preco: 5,
    meme: 15,
    effect: "escolha alguém, ambos -1 Potássio",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543196129329232/trunfo18.png",
  },
  {
    id: 19,
    qt_macaco: 4,
    potassio: 10,
    preco: 16,
    meme: 8,
    effect: "Uma pessoa -1 em Qt e você +1 em qualquer",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543204568924230/trunfo19_.png",
  },
  {
    id: 20,
    qt_macaco: 12,
    potassio: 8,
    preco: 5,
    meme: 9,
    effect: "-3 de Qt macaco de alguém",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543213305004032/trunfo20.png",
  },
  {
    id: 21,
    qt_macaco: 3,
    potassio: 8,
    preco: 13,
    meme: 16,
    effect: "+7 em Qt macacos",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543247632367616/trunfo21.png",
  },
  {
    id: 22,
    qt_macaco: 3,
    potassio: 6,
    preco: 11,
    meme: 17,
    effect: "+3 em qualquer",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543256973082694/trunfo22.png",
  },
  {
    id: 23,
    qt_macaco: 10,
    potassio: 4,
    preco: 6,
    meme: 19,
    effect: "-1 de alguém volta para você",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543264078102568/trunfo23.png",
  },
  {
    id: 24,
    qt_macaco: 10,
    potassio: 4,
    preco: 19,
    meme: 5,
    effect: "-3 em preço todos",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543270486867968/trunfo24.png",
  },
  {
    id: 25,
    qt_macaco: 9,
    potassio: 9,
    preco: 5,
    meme: 15,
    effect: "+5 em preço",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543277540900864/trunfo25.png",
  },
  {
    id: 26,
    qt_macaco: 13,
    potassio: 9,
    preco: 9,
    meme: 11,
    effect: "-2 Meme + 1 preço",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543288208064522/trunfo26.png",
  },
  {
    id: 27,
    qt_macaco: 13,
    potassio: 19,
    preco: 4,
    meme: 4,
    effect: "Meme ou preço x2",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543303756349450/trunfo27.png",
  },
  {
    id: 28,
    qt_macaco: 7,
    potassio: 10,
    preco: 4,
    meme: 6,
    effect: "+3 em qualquer",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543311297445918/Trunfo28.png",
  },
  {
    id: 29,
    qt_macaco: 9,
    potassio: 8,
    preco: 13,
    meme: 7,
    effect: "Um status + 3 e outro +1",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543319832199178/trunfo29.png",
  },
  {
    id: 30,
    qt_macaco: 7,
    potassio: 3,
    preco: 19,
    meme: 8,
    effect: "adversários perdem 2 em status",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543330099855370/trunfo30.png",
  },
  {
    id: 31,
    qt_macaco: 12,
    potassio: 2,
    preco: 12,
    meme: 6,
    effect: "+2 em qualquer status",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543368272347196/trunfo31.png",
  },
  {
    id: 32,
    qt_macaco: 6,
    potassio: 6,
    preco: 12,
    meme: 8,
    effect: "Duas pessoas -2 Qt macacos",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543383741464577/trunfo32.png",
  },
  {
    id: 33,
    qt_macaco: 5,
    potassio: 4,
    preco: 10,
    meme: 10,
    effect: "escolha pessoa para tirar de outra -2 Qt macaco",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543394172960818/Trunfo33.png",
  },
  {
    id: 34,
    qt_macaco: 11,
    potassio: 4,
    preco: 7,
    meme: 8,
    effect: "Duas perdem -1 em tudo",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543405560365126/Trunfo34.png",
  },
  {
    id: 35,
    qt_macaco: 9,
    potassio: 4,
    preco: 6,
    meme: 8,
    effect: "Adversários perdem -2 em status que quiser",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543412459601961/Trunfo35.png",
  },
  {
    id: 36,
    qt_macaco: 7,
    potassio: 19,
    preco: 10,
    meme: 5,
    effect: "-2 você e outra pessoa",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814544172915621919/Trunfo36.png",
  },
  {
    id: 37,
    qt_macaco: 7,
    potassio: 8,
    preco: 10,
    meme: 9,
    effect: "Adversários -1 em tudo você +1 meme",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543420159950848/Trunfo37.png",
  },
  {
    id: 38,
    qt_macaco: 4,
    potassio: 3,
    preco: 16,
    meme: 12,
    effect: "escolha alguém -3 em Potássio e você +1 Qt macacos",
    url_image:
      "https://cdn.discordapp.com/attachments/746739491217080340/814543431786692648/Trunfo38.png",
  },
  {
    id: 39,
    qt_macaco: 2,
    potassio: 12,
    preco: 11,
    meme: 9,
    effect: "Ganhe +2 em algum status",
    url_image:
      "https://cdn.discordapp.com/attachments/733117754168574002/1033803853121978408/Trunfo39.png",
  },
  {
    id: 40,
    qt_macaco: 7,
    potassio: 2,
    preco: 10,
    meme: 8,
    effect: "Troque um status base com alguém",
    url_image:
      "https://cdn.discordapp.com/attachments/733117754168574002/1033803853482709113/Trunfo40.png",
  },
  {
    id: 41,
    qt_macaco: 3,
    potassio: 8,
    preco: 10,
    meme: 9,
    effect: "+3 em qualquer status e todos perdem -1",
    url_image:
      "https://cdn.discordapp.com/attachments/733117754168574002/1033803853881151599/Trunfo41.png",
  },
  {
    id: 42,
    qt_macaco: 7,
    potassio: 13,
    preco: 7,
    meme: 6,
    effect: "+3 em qualquer status",
    url_image:
      "https://cdn.discordapp.com/attachments/733117754168574002/1033803854254456852/Trunfo42.png",
  },
  {
    id: 43,
    qt_macaco: 4,
    potassio: 5,
    preco: 10,
    meme: 18,
    effect: "+1 em qualquer status e escolha alguém para tirar -2 seu",
    url_image:
      "https://media.discordapp.net/attachments/733117754168574002/1033803854669680720/Trunfo43.png",
  },
  {
    id: 44,
    qt_macaco: 8,
    potassio: 6,
    preco: 9,
    meme: 14,
    effect: "Perca -1 QT + 1 Potassio",
    url_image:
      "https://media.discordapp.net/attachments/733117754168574002/1033803855051358298/Trunfo44.png",
  },
  {
    id: 45,
    qt_macaco: 6,
    potassio: 4,
    preco: 19,
    meme: 7,
    effect: "+2 em um status e -2 de preço de outra",
    url_image:
      "https://media.discordapp.net/attachments/733117754168574002/1033803855370133544/Trunfo45.png",
  },
  {
    id: 46,
    qt_macaco: 3,
    potassio: 13,
    preco: 10,
    meme: 6,
    effect: "+3 em um status -3 em outro e tire -3 de alguém",
    url_image:
      "https://media.discordapp.net/attachments/733117754168574002/1033803855722450944/Trunfo46.png",
  },
  {
    id: 47,
    qt_macaco: 10,
    potassio: 5,
    preco: 5,
    meme: 11,
    effect: "+2 em preço ou potássio e alguém -2 no mesmo",
    url_image:
      "https://media.discordapp.net/attachments/733117754168574002/1033803856058007623/Trunfo47.png",
  },
];
