import React, { useContext, useState } from "react";
import { cards } from "../../cards_example";
import { GlobalContext } from "../../contexts/GlobalContext";
import { language } from "../../language";
import Card from "../Card";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";

function UserForm({ newId }: { newId: number }) {
  const { globalData, dispatchGlobal } = useContext(GlobalContext);
  const [UserInfo, setCard] = useState<{
    username: string;
  }>({
    username: "",
  });

  return (
    <div className='block__user--form'>
      <TextField
        type='text'
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setCard((oldData) => ({
            ...oldData,
            ...{ username: e.target.value },
          }));
        }}
        label='Nome'
        placeholder={`${language[globalData.language]["name_player"]}`}
        value={UserInfo.username}
      />
      <input
        type='number'
        placeholder={`${language[globalData.language]["id_card"]}`}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          dispatchGlobal({
            type: "ADD_NUMBER_CARD",
            payload: { idInput: newId, idCard: e.target.value },
          });
        }}
        min='0'
        max='40'
        step='1'
        value={globalData.round[newId] || ""}
      />
      {globalData.round[newId] !== 0 &&
      cards.length + 1 > globalData.round[newId] ? (
        <Card
          data={cards.filter((card) => card.id === globalData.round[newId])[0]}
          user={UserInfo.username}
        />
      ) : null}
      {globalData.round[newId] > 0 && (
        <Button
          onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            dispatchGlobal({
              type: "REMOVE_CARD",
              payload: { user: UserInfo.username, newId: newId },
            });
          }}
          style={{ marginTop: "5px" }}
        >
          <DeleteIcon fontSize='small' style={{ fill: "white" }} />
        </Button>
      )}
    </div>
  );
}

export default UserForm;
