import React, { useState } from "react";
import { cards } from "../../cards_example";
import Card from "../Card";
import TextField from "@material-ui/core/TextField";
import {ButtonMonkey} from "../ButtonMonkey";
import { ICard } from "../Card/Card";

function ShuffleApp() {
  const [deck, setDeck] = useState<{ [key: string]: ICard[] }>({});
  const [handNumber, setHandNumbers] = useState(0);
  const [numberPlayers, setNumber] = useState(0);
  const genCard = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    let arrayPlayers = Array.from({ length: numberPlayers }, (v, id) => {
      return id;
    });
    const newDecks = shuffly(cards);
    let newPlayer: { [key: string]: ICard[] } = {};
    for (let player in arrayPlayers) {
      let firstNumber = parseInt(player) * handNumber;
      let lastNumber = (parseInt(player) + 1) * handNumber;
      newPlayer[player] = newDecks.slice(firstNumber, lastNumber);
    }
    setDeck(newPlayer);
  };

  return (
    <div className='block--deck'>
      <TextField
        type='number'
        label='Número de jogadores:'
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setNumber(parseInt(e.target.value));
        }}
      />
      <TextField
        type='number'
        label='Numero de cartas na mão:'
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setHandNumbers(parseInt(e.target.value));
        }}
      />
      <ButtonMonkey fnClick={genCard}>Generate Deck</ButtonMonkey>
      {Object.keys(deck).length > 0 &&
        Array.from({ length: numberPlayers }, (v, id) => {
          return (
            <section className='block--only-card'>
              <h3 className='block--player-title'>Jogador {id + 1}</h3>
              {deck[id]?.map((item) => (
                <Card data={item} user={""} onlyCard={true} />
              ))}
            </section>
          );
        })}
    </div>
  );
}

function shuffly(sourceArray: ICard[]) {
  for (var i = 0; i < sourceArray.length - 1; i++) {
    var j = i + Math.floor(Math.random() * (sourceArray.length - i));
    var temp = sourceArray[j];
    sourceArray[j] = sourceArray[i];
    sourceArray[i] = temp;
  }
  return sourceArray;
}

export default ShuffleApp;
