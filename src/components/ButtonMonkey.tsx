import React from "react";
import { withStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";

const StyledButton = withStyles({
  root: {
    background: "green",
    borderRadius: 2,
    border: 0,
    color: "white",
    height: 30,
    padding: "0 30px",
    marginTop: "20px",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);


const StyleControl = withStyles({
  root: {
    background: "green",
    borderRadius: 2,
    border: 0,
    color: "white",
		minWidth: 22,
    padding: "0",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

export const ButtonMonkey: React.FC<{
  children: React.ReactNode;
  fnClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
}> = ({ children,  fnClick }) => {
  return <StyledButton onClick={fnClick}>{children}</StyledButton>;
	};

export const ButtonControl: React.FC<{
  children: React.ReactNode;
  fnClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
}>=({children, fnClick}) => {
	return <StyleControl onClick={fnClick}>{children}</StyleControl>
}
