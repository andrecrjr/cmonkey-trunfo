import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../../contexts/GlobalContext";
import { ICard } from "./Card";
import { language } from "../../language";
import { InputCard } from "./InputCard";
import { Button } from "@material-ui/core";

export default function Card({
  data,
  user,
  onlyCard,
}: {
  data: ICard;
  user: string;
  onlyCard?: boolean;
}) {
  const { dispatchGlobal, globalData } = useContext(GlobalContext);
  const [updatedData, setUpdatedData] = useState<ICard>({ ...data, user });
  useEffect(() => {
    setUpdatedData({ ...data, user });
  }, [user, data]);

  if (onlyCard) {
    return (
      <div className='block__card'>
        <div className='block__card--main'>
          <div className='block__card--image'>
            <p className='block__card--card'>
              {`${language[globalData.language]["card"]}`}: {data.id}
            </p>
            <a href={`${data.url_image}`} target='blank'>
              <img src={`${data.url_image}`} alt={data.effect} />
            </a>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className='block__card'>
      <h1>
        {language[globalData.language]["player"]}: {user}
      </h1>
      {(user.length > 0 && data && (
        <div className='block__card--main'>
          <div className='block__card--image'>
            <p>
              {`${language[globalData.language]["card"]}`}: {data.id || ""}
            </p>
            {data.url_image !== "" ? (
              <a href={data.url_image} target='blank'>
                <img src={data.url_image} alt={data.effect} />
              </a>
            ) : null}
            <p className='block__image--effect'>{data.effect}</p>
          </div>

          <section className='block__card--info'>
            <InputCard
              tipo={{
                slug: "meme",
                nome: language[globalData.language]["meme"],
              }}
              user={user}
              updater={setUpdatedData}
              status={data.meme}
            />
            <InputCard
              tipo={{
                slug: "potassio",
                nome: language[globalData.language]["potassio"],
              }}
              updater={setUpdatedData}
              user={user}
              status={data.potassio}
            />
            <InputCard
              tipo={{
                slug: "preco",
                nome: language[globalData.language]["preco"],
              }}
              user={user}
              updater={setUpdatedData}
              status={data.preco}
            />
            <InputCard
              tipo={{
                slug: "qt_macaco",
                nome: language[globalData.language]["qt_macaco"],
              }}
              user={user}
              updater={setUpdatedData}
              status={data.qt_macaco}
            />
            <Button
              variant='contained'
              style={{ fontSize: "13px", marginTop: "5px" }}
              onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
                e.preventDefault();
                dispatchGlobal({ type: "SET_EFFECT", payload: updatedData });
              }}
            >
              {language[globalData.language]["close_card_round"]}
            </Button>
          </section>
        </div>
      )) || <div></div>}
    </div>
  );
}
