import { useState, useEffect } from "react";
import { ICard } from "./Card";
import {TextField} from "@material-ui/core";
import { ButtonControl } from "../ButtonMonkey";

interface IInputCard {
  tipo: { nome: string; slug: string };
  user: string;
  status: number;
  updater: (data: ICard | ((oldData: ICard) => ICard)) => void;
}

export const InputCard = ({ tipo, user, status, updater }: IInputCard) => {
  const [newStatus, setNewStatus] = useState<number>(status);
  useEffect(() => {
    setNewStatus(status);
  }, [status]);

  const calcBonus = (action: "+" | "-") => {
    let value: { [k: string]: number } = {};
    value[tipo.slug] = newStatus;
    switch (action) {
      case "-":
        setNewStatus(newStatus - 1);
        value[tipo.slug] = newStatus - 1;
        updater((oldData) => ({ ...oldData, ...value }));
        break;
      case "+":
        setNewStatus(newStatus + 1);
        value[tipo.slug] = newStatus + 1;
        updater((oldData) => ({ ...oldData, ...value }));
        break;
      default:
        setNewStatus(newStatus);
        value[tipo.slug] = newStatus;
        updater((oldData) => ({ ...oldData, ...value }));
        break;
    }
  };

  return (
    <section className='block--number-card'>
      <label htmlFor={`${tipo.slug}-${status}-${user}`}>{tipo.nome}: </label>
			<ButtonControl fnClick={()=>{calcBonus("-")}}>-</ButtonControl>
      <TextField
        type='number'
        id={`${tipo.slug}-${status}-${user}`}
        value={newStatus}
        onScroll={(e: React.WheelEvent<HTMLInputElement>) => {
          e.preventDefault();
        }}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setNewStatus(Number(e.target.value));
          let value: { [k: string]: number } = {};
          value[tipo.slug] = Number(e.target.value);
          updater((oldData) => ({ ...oldData, ...value }));
        }}
      />
      <ButtonControl fnClick={() => calcBonus("+")}>+</ButtonControl>
    </section>
  );
};
