export interface ICard {
  [key: string]: any;
  user?: string;
  id: number;
  effect: string;
  qt_macaco: number;
  potassio: number;
  meme: number;
  preco: number;
  url_image?: string;
}
