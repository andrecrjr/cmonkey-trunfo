import React from "react";
import { language } from "../../language";
import { Link } from "react-router-dom";

const Layout: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  return (
    <>
      <header>
        <h3>Cryptomonkey Trunfo Admin</h3>
        <section>
          <ul className='menu'>
            <li>
              <Link to='/'>{language["pt-br"]["adminGame"]}</Link>
            </li>
            <li>
              <Link to='/shuffle-cards'>{language["pt-br"]["mountDeck"]}</Link>
            </li>
          </ul>
        </section>
      </header>
      {children}
    </>
  );
};

export default Layout;
