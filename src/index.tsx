import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import ShuffleApp from "./components/Shuffle";
import App from "./App";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Layout from "./components/Layout";

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Layout>
        <Route exact path='/'>
          <App />
        </Route>
        <Route path='/shuffle-cards'>
          <ShuffleApp />
        </Route>
      </Layout>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
