export const language: { [index: string]: any } = {
  "pt-br": {
    number_player: "Número de Jogadores",
    name_player: "Nome do Jogador",
    player: "Jogador",
    used_bonus: "Usou bônus",
    potassio: "Potássio",
    qt_macaco: "Quant. Macaco",
    meme: "Meme",
    preco: "Preço",
    att_winner: "Atributo vencedor",
    att_chosen: "Atributo escolhido",
    id_card: "Id da carta",
    close_card_round: "Fase Bônus concluída",
		card: "Carta",
		mountDeck: "Embaralhador de cartas",
		adminGame:"Admin de Partida"
  },
  es: {
    number_player: "Numbre de Jugadores",
    potassio: "Potássium",
    qt_monkey: "Quant. macacos",
  },
};
