import { createContext } from "react";
import { IGlobalState, initialReducer } from "../reducers";

export const GlobalContext = createContext<{
  globalData: IGlobalState;
  dispatchGlobal: React.Dispatch<any>;
}>({
  globalData: initialReducer,
  dispatchGlobal: () => null,
});
