import { ICard } from "../components/Card/Card";

export interface IGlobalState {
  attributeWinner: string;
  cards: ICard[];
  round: { [key: number]: any };
  winner: {
    user: string;
    attribute: string;
    value: number;
  }[];
  language: "pt-br" | "es";
}

export const initialReducer: IGlobalState = {
  attributeWinner: "",
  cards: [],
  winner: [
    {
      user: "",
      attribute: "",
      value: 0,
    },
  ],
  round: { 0: 0 },
  language: "pt-br",
};

type IActions =
  | { type: "SET_EFFECT"; payload: ICard }
  | { type: "SET_WINNER"; payload: string }
  | { type: "SET_WINNER_ATT"; payload: string }
  | { type: "GET_FINAL_ROUND_RESULT" }
  | { type: "RESET_STATE"; payload: IGlobalState }
  | { type: "ADD_ME"; payload: number }
  | { type: "REMOVE_CARD"; payload: { user: string; newId: number } }
  | { type: "ADD_NUMBER_CARD"; payload: { idInput: number; idCard: number } };

export function GlobalReducer(
  state: IGlobalState,
  action: IActions
): IGlobalState {
  switch (action.type) {
    case "SET_EFFECT":
      let newState = state;

      const alreadyHasUser = state.cards.some(
        (card) => card.user === action.payload.user
      );
      if (alreadyHasUser) {
        newState = {
          ...state,
          cards: state.cards.map((card, index) => {
            if (card.user === action.payload.user) {
              console.log(card.id);
              return { ...card, ...action.payload };
            }
            return { ...card };
          }),
        };
      } else {
        newState = { ...state, cards: [...state.cards, action.payload] };
      }
      return newState;
    case "SET_WINNER_ATT":
      return { ...state, attributeWinner: action.payload };
    case "GET_FINAL_ROUND_RESULT":
      let tempVar: {
        user: string;
        attribute: string;
        value: number;
      }[] = [];
      for (let card of state.cards) {
        let user: string = card.user || "";
        const attributeCard: number = card[state.attributeWinner];
        tempVar.push({
          user,
          value: attributeCard,
          attribute: state.attributeWinner,
        });
      }
      const ranking = tempVar
        .sort(function (a, b) {
          return a.value - b.value;
        })
        .reverse();

      return {
        ...state,
        winner: ranking,
      };
    case "RESET_STATE":
      return { ...state, ...action.payload };
    case "ADD_ME":
      let newRound: { [key: number]: any } = {};
      newRound[action.payload] = "";
      return { ...state, round: { ...state.round, ...newRound } };
    case "ADD_NUMBER_CARD":
      let updateRound: { [key: number]: any } = {};
      updateRound[action.payload.idInput] = Number(action.payload.idCard);
      return { ...state, round: { ...state.round, ...updateRound } };
    case "REMOVE_CARD":
      let removeCard: { [key: number]: any } = {};
      removeCard[action.payload.newId] = "";
      return {
        ...state,
        cards: state.cards.filter(
          (card) => card.user !== (action.payload.user || "")
        ),
        round: { ...state.round, ...removeCard },
      };
    default:
      return state;
  }
}
