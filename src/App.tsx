import React, { useReducer, useState } from "react";
import "./App.css";
import UserForm from "./components/UserForm";
import { GlobalContext } from "./contexts/GlobalContext";
import { language } from "./language";
import TextField from "@material-ui/core/TextField";
import { GlobalReducer, initialReducer } from "./reducers";
import { Button } from "@material-ui/core";

function App() {
  const [globalData, dispatchGlobal] = useReducer(
    GlobalReducer,
    initialReducer
  );
  const [numberUserForms, setUserNumbers] = useState(4);
  const resetState = (e: React.MouseEvent<HTMLButtonElement>) => {
    dispatchGlobal({
      type: "RESET_STATE",
      payload: { ...initialReducer, language: "pt-br" },
    });
  };
  return (
    <div className='App'>
      {globalData.winner.length === numberUserForms &&
        globalData.attributeWinner !== "" && (
          <div>
            <h1>Ranking:</h1>
            <p className='white'>
              Atributo:{" "}
              {language[globalData.language][globalData.attributeWinner]}
            </p>
            <ul>
              {globalData.winner.map((item, index) => {
                return (
                  <li className='white'>
                    {index + 1}°: {item.user} - {item.value} pts.
                  </li>
                );
              })}
            </ul>
            <button onClick={resetState}>Apagar rodada</button>
          </div>
        )}
      <section>
        <label htmlFor='number-players'>Numero de jogadores:</label>
        <TextField
          type='number'
          name=''
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setUserNumbers(parseInt(e.target.value));
          }}
          placeholder='Jogadores'
          id='number-players'
        />
      </section>
      <section className='app--main'>
        <GlobalContext.Provider value={{ globalData, dispatchGlobal }}>
          {Array.from({ length: numberUserForms }, (v, id) => {
            return (
              <div style={{ display: "contents" }} key={id}>
                <UserForm newId={id} />
              </div>
            );
          })}
        </GlobalContext.Provider>
      </section>
      <div className='att--winner'>
        <label htmlFor='attribute-winner'>
          {language[globalData.language]["att_chosen"]}:
        </label>
        <select
          name=''
          id='attribute-winner'
          onChange={(e: React.FormEvent<HTMLSelectElement>) => {
            dispatchGlobal({
              type: "SET_WINNER_ATT",
              payload: e.currentTarget.value,
            });
          }}
        >
          <option value='choice' selected={globalData.attributeWinner === ""}>
            Selecione atributo
          </option>
          <option value='qt_macaco'>
            {language[globalData.language]["qt_macaco"]}
          </option>
          <option value='preco'>
            {language[globalData.language]["preco"]}
          </option>
          <option value='potassio'>
            {language[globalData.language]["potassio"]}
          </option>
          <option value='meme'> {language[globalData.language]["meme"]}</option>
        </select>
        {numberUserForms === globalData.cards.length && (
          <Button
            onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
              dispatchGlobal({ type: "GET_FINAL_ROUND_RESULT" });
              window.scrollTo(0, 0);
            }}
          >
            Vencedor
          </Button>
        )}
      </div>
    </div>
  );
}

export default App;
